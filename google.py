# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
url = "http://www.google.com.mx/search?q=gbatemp&oq=gbatemp&aqs=chrome..69i57j69i60l2j69i59j0l2.807j0j4&sourceid=chrome&ie=UTF-8"

# Realizamos la petición a la web
req = requests.get(url)

# Comprobamos que la petición nos devuelve un Status Code = 200
statusCode = req.status_code
if statusCode == 200:

    # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
    html = BeautifulSoup(req.text)

    # Obtenemos todos los divs donde estan las entradas
    entradas = html.find_all('div',{'class':'g'})

    # Recorremos todas las entradas para extraer el título, autor y fecha
    for i,entrada in enumerate(entradas):
     # Con el método "getText()" no nos devuelve el HTML
          titulo = entrada.find('h3',{'class':'r'}).getText()
        # Sino llamamos al método "getText()" nos devuelve también el HTML
          url = entrada.find('cite').getText()
          desc = entrada.find('span', {'class' : 'st'}).getText()

          print ("Busqueda:",str(i+1),":\n",titulo,"\n------\n",url,"\n---------------\n",desc,"\n==========================")
else:
    print ("Status Code %d" %statusCode)
