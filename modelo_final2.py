from peewee import *
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

database = MySQLDatabase("ana_seguros", host='192.168.1.222', port=3306, user='root', password='')

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database



class AutosAna(BaseModel):
    descripcion = CharField()
    id_autocompara = IntegerField()
    id_compara = IntegerField()
    id_marca_ana = IntegerField()
    id_modelo_ana = IntegerField()
    id_wibe = IntegerField()

    class Meta:
        db_table = 'autos_ana'

class MarcaAc(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_ac'

class MarcaAna(BaseModel):
    marca = CharField()

    class Meta:
        db_table = 'marca_ana'

class MarcaSi(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_si'

class MarcaWibe(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_wibe'

class ModeloAc(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_ac'

class ModeloAna(BaseModel):
    modelo = IntegerField()

    class Meta:
        db_table = 'modelo_ana'

class ModeloSi(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_si'

class ModeloWibe(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_wibe'

class SubmarcaWibe(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'submarca_wibe'

class VehiculoAc(BaseModel):
    descripcion = CharField()
    error = IntegerField(null=True)
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = IntegerField()
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_ac'

class VehiculoSi(BaseModel):
    descripcion = CharField()
    error = IntegerField()
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = IntegerField()
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_si'

class VehiculoWibe(BaseModel):
    descripcion = CharField()
    error = IntegerField()
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = CharField()
    submarca = IntegerField(db_column='submarca_id', index=True)
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_wibe'


i=1
while i <= AutosAna.select().order_by(AutosAna.id.desc()).get().id:
  aux = AutosAna.select().where(AutosAna.id == i).get().id_compara
  if aux==0:
    ana_modelo = AutosAna.select().where(AutosAna.id==i).get().id_modelo_ana
    ana_modelo2 = ModeloAna.select().where(ModeloAna.id==ana_modelo).get().modelo
    #print "Ana_Modelo:",ana_modelo2
    ana_marca = AutosAna.select().where(AutosAna.id == i).get().id_marca_ana
    ana_marca2 = MarcaAna.select().where(MarcaAna.id == ana_marca).get().marca
    #print "Ana_Marca:", ana_marca2

    desc_ana = AutosAna.select().where(AutosAna.id == i).get().descripcion
    print ">:",i, ana_modelo2, ana_marca2, desc_ana

    # print":::::::::::::::::::::::::::::::::::::::::::::::::::::::"
    #
    # print "Analisis num: ",i," Seguros inteligentes"
    # print":::::::::::::::::::::::::::::::::::::::::::::::::::::::"
    string_ana = str(ana_modelo2), str(ana_marca2), desc_ana
    isegint = 1
    id_veisi = 0
    ac_ratio = 0

    while isegint < VehiculoSi.select().order_by(VehiculoSi.id.desc()).get().id:
        try:
            # Obtener modelo Seguro Inteligente
            veisi_modelo_id = VehiculoSi.select().where(VehiculoSi.id == isegint).get().modelo
            veisi_marca_id = VehiculoSi.select().where(VehiculoSi.id == isegint).get().marca
            veisi_descr = VehiculoSi.select().where(VehiculoSi.id == isegint).get().descripcion
            # print("Modelo Seguro inteligente: ", veisi_modelo2)
            # Obtener Marca Seguro Inteligente
            if ModeloSi.select().where(ModeloSi.year==ana_modelo2).exists():
                veisi_modelo2 = ModeloSi.select().where(ModeloSi.id == veisi_modelo_id).get().year

            else:
                print("No existe el modelo!")
                break
            if MarcaSi.select().where(MarcaSi.name==ana_marca2).exists():
                veisi_marca2 = MarcaSi.select().where(MarcaSi.id == veisi_marca_id).get().name
            else:
                print("No exisgte la marca!")
                break


           #print">:", isegint, veisi_modelo2, veisi_marca2, veisi_descr


            string_veisi = str(veisi_modelo2), str(veisi_marca2), veisi_descr

            ac_ratio_aux = fuzz.token_set_ratio(string_ana, string_veisi)

            if (ac_ratio_aux > ac_ratio):
                id_veisi = VehiculoSi.select().where(VehiculoSi.id == isegint).get().id
                ac_ratio = ac_ratio_aux
                print">:", isegint, veisi_modelo2, veisi_marca2, veisi_descr
                print "RATIO:", ac_ratio

            isegint += 1

        except:
            print "valor vacio en Seguro inteligente"
            isegint += 1

    update = AutosAna.update(id_compara=id_veisi).where(AutosAna.id == i)
    update.execute()

    print"Analisis num: ",i, "Finalizado"


  else:
      print "Ya tiene valor"
  i += 1