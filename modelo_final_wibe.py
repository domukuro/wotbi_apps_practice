from peewee import *
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

database = MySQLDatabase("ana_seguros", host='192.168.1.222', port=3306, user='root', password='')

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database



class AutosAna(BaseModel):
    descripcion = CharField()
    id_autocompara = IntegerField()
    id_compara = IntegerField()
    id_marca_ana = IntegerField()
    id_modelo_ana = IntegerField()
    id_wibe = IntegerField()

    class Meta:
        db_table = 'autos_ana'

class MarcaAc(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_ac'

class MarcaAna(BaseModel):
    marca = CharField()

    class Meta:
        db_table = 'marca_ana'

class MarcaSi(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_si'

class MarcaWibe(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_wibe'

class ModeloAc(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_ac'

class ModeloAna(BaseModel):
    modelo = IntegerField()

    class Meta:
        db_table = 'modelo_ana'

class ModeloSi(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_si'

class ModeloWibe(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_wibe'

class SubmarcaWibe(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'submarca_wibe'

class VehiculoAc(BaseModel):
    descripcion = CharField()
    error = IntegerField(null=True)
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = IntegerField()
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_ac'

class VehiculoSi(BaseModel):
    descripcion = CharField()
    error = IntegerField()
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = IntegerField()
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_si'

class VehiculoWibe(BaseModel):
    descripcion = CharField()
    error = IntegerField()
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = CharField()
    submarca = IntegerField(db_column='submarca_id', index=True)
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_wibe'


i=1

while i <= AutosAna.select().order_by(AutosAna.id.desc()).get().id:
  aux = AutosAna.select().where(AutosAna.id == i).get().id_wibe
  if aux==0:
    ana_modelo = AutosAna.select().where(AutosAna.id==i).get().id_modelo_ana
    ana_modelo2 = ModeloAna.select().where(ModeloAna.id==ana_modelo).get().modelo
    #print "Ana_Modelo:",ana_modelo2
    ana_marca = AutosAna.select().where(AutosAna.id == i).get().id_marca_ana
    ana_marca2 = MarcaAna.select().where(MarcaAna.id == ana_marca).get().marca
    #print "Ana_Marca:", ana_marca2

    desc_ana = AutosAna.select().where(AutosAna.id == i).get().descripcion
    print ">:",i, ana_modelo2, ana_marca2, desc_ana

    print":::::::::::::::::::::::::::::::::::::::::::::::::::::::"

    print "Analisis num: ",i," Seguros inteligentes"
    print":::::::::::::::::::::::::::::::::::::::::::::::::::::::"
    string_ana = str(ana_modelo2), str(ana_marca2), desc_ana
    iwibe = 1
    id_wibe = 0
    ac_ratio = 0

    while iwibe <= VehiculoWibe.select().order_by(VehiculoWibe.id.desc()).get().id:
        try:
            # Obtener modelo Wibe
            vewibe_modelo_id = VehiculoWibe.select().where(VehiculoWibe.id == iwibe).get().modelo
            vewibe_marca_id = VehiculoWibe.select().where(VehiculoWibe.id == iwibe).get().marca
            vewibe_submarca_id = VehiculoWibe.select().where(VehiculoWibe.id == iwibe).get().submarca
            vewibe_descr = VehiculoWibe.select().where(VehiculoWibe.id == iwibe).get().descripcion
            #Aqui se concatena papu :v
            subplusdescr=str(vewibe_submarca_id) + " " + str(vewibe_descr)
            # print "subplusdescr",subplusdescr

            # Obtener Marca Wibe
            if ModeloWibe.select().where(ModeloWibe.year==ana_modelo2).exists():
                vewibe_modelo2 = ModeloWibe.select().where(ModeloWibe.id == vewibe_modelo_id).get().year

            else:
                print("No existe el modelo!")
                break
            if MarcaWibe.select().where(MarcaWibe.name==ana_marca2).exists():
                vewibe_marca2 = MarcaWibe.select().where(MarcaWibe.id == vewibe_marca_id).get().name
            else:
                print("No exisgte la marca!")
                break


           #print">:", isegint, veisi_modelo2, veisi_marca2, veisi_descr


            string_vewibe = str(vewibe_modelo2), str(vewibe_marca2), subplusdescr

            ac_ratio_aux = fuzz.partial_ratio(string_ana, string_vewibe)

            if (ac_ratio_aux > ac_ratio):
                id_wibe = VehiculoSi.select().where(VehiculoSi.id == iwibe).get().id
                ac_ratio = ac_ratio_aux
                print "wibe>",string_vewibe
                print "RATIO:", ac_ratio

            iwibe += 1

        except:
            print "valor vacio en WIBE",iwibe
            iwibe += 1

    update = AutosAna.update(id_wibe=id_wibe).where(AutosAna.id == i)
    update.execute()

    print"Analisis num: ",i, "Finalizado"
  else:
    print "Dato existente:",i
    i+=1
