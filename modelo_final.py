from peewee import *
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

database = MySQLDatabase('ana_seguros', **{'user': 'root'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database



class AutosAna(BaseModel):
    descripcion = CharField()
    id_autocompara = IntegerField()
    id_compara = IntegerField()
    id_marca_ana = IntegerField()
    id_modelo_ana = IntegerField()
    id_wibe = IntegerField()

    class Meta:
        db_table = 'autos_ana'

class MarcaAc(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_ac'

class MarcaAna(BaseModel):
    marca = CharField()

    class Meta:
        db_table = 'marca_ana'

class MarcaSi(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_si'

class MarcaWibe(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'marca_wibe'

class ModeloAc(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_ac'

class ModeloAna(BaseModel):
    modelo = IntegerField()

    class Meta:
        db_table = 'modelo_ana'

class ModeloSi(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_si'

class ModeloWibe(BaseModel):
    year = IntegerField()

    class Meta:
        db_table = 'modelo_wibe'

class SubmarcaWibe(BaseModel):
    name = CharField()
    number = IntegerField()

    class Meta:
        db_table = 'submarca_wibe'

class VehiculoAc(BaseModel):
    descripcion = CharField()
    error = IntegerField(null=True)
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = IntegerField()
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_ac'

class VehiculoSi(BaseModel):
    descripcion = CharField()
    error = IntegerField()
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = IntegerField()
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_si'

class VehiculoWibe(BaseModel):
    descripcion = CharField()
    error = IntegerField()
    marca = IntegerField(db_column='marca_id', index=True)
    modelo = IntegerField(db_column='modelo_id', index=True)
    number = CharField()
    submarca = IntegerField(db_column='submarca_id', index=True)
    tipo = IntegerField(db_column='tipo_id', index=True)

    class Meta:
        db_table = 'vehiculo_wibe'


i=1
while i <= AutosAna.select().order_by(AutosAna.id.desc()).get().id:
  aux = AutosAna.select().where(AutosAna.id== i).get().id_autocompara
  # print "Aux",aux
  if aux==0:
    ana_modelo = AutosAna.select().where(AutosAna.id==i).get().id_modelo_ana
    ana_modelo2 = ModeloAna.select().where(ModeloAna.id==ana_modelo).get().modelo
    #print "Ana_Modelo:",ana_modelo2
    ana_marca = AutosAna.select().where(AutosAna.id == i).get().id_marca_ana
    ana_marca2 = MarcaAna.select().where(MarcaAna.id == ana_marca).get().marca
    #print "Ana_Marca:", ana_marca2

    desc_ana = AutosAna.select().where(AutosAna.id == i).get().descripcion
    print ">:",i, ana_modelo2, ana_marca2, desc_ana

    i_ac=1

    id_ac=0
    ac_ratio=0
    #string_ana=str(ana_modelo2)+" "+str(ana_marca2)+" "+desc_ana
    string_ana = str(ana_modelo2), str(ana_marca2), desc_ana
    while i_ac < VehiculoAc.select().order_by(VehiculoAc.id.desc()).get().id:
     try:

        desc_ac = VehiculoAc.select().where(VehiculoAc.id == i_ac).get().descripcion
        id_modelo_ac = VehiculoAc.select().where(VehiculoAc.id == i_ac).get().modelo
        id_marca_ac  = VehiculoAc.select().where(VehiculoAc.id == i_ac).get().marca
        if ModeloAc.select().where(ModeloAc.year == ana_modelo2).exists():
            ac_modelo = ModeloAc.select().where(ModeloAc.id==id_modelo_ac).get().year
        else:
            print "No existe el modelo!"
            break

        if MarcaAc.select().where(MarcaAc.name==ana_marca2).exists():
            ac_marca = MarcaAc.select().where(MarcaAc.id==id_marca_ac).get().name
        else:
            print "No existe la marca!"
            break

        #string_ac = str(ac_modelo)+" "+str(ac_marca)+" "+desc_ac
        string_ac = str(ac_modelo), str(ac_marca), desc_ac
        # print "String_ana:\t",string_ana
        # print "String_aC:\t", string_ac
        ac_ratio_aux=fuzz.ratio(string_ana,string_ac)
        # print "Fuzz:",ac_ratio_aux
        #raw_input("")
        if(ac_ratio_aux>ac_ratio):
            id_ac = VehiculoAc.select().where(VehiculoAc.id == i_ac).get().id
            ac_ratio=ac_ratio_aux
            print "RATIO:",ac_ratio
            #if ac_ratio>90:
            #    raw_input("")
        i_ac += 1
     except:
         print "valor vacio en autocompara"
         i_ac += 1
    update = AutosAna.update(id_autocompara=id_ac).where(AutosAna.id==i)
    update.execute()
    print"Update realizado"
  else:
      print i,"> Ya tiene registro"
  i+=1
